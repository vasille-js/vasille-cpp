#include <functional>
#include <unordered_set>
#include <vasille/core/ivalue.h++>

#pragma once

template <typename T>
class Reference : public IValue<T>
{
    T value;

    std::unordered_set<const std::function<void(const T &)> *> onchange;

public:
    Reference()
        : IValue<T>(true) {}

    Reference(const T & value)
        : IValue<T>(true) {
        this->value = value;
    }

    // IValue interface


    operator const T &() override {
        return value;
    }

    void operator=(const T & value) override {

        if (this->value != value) {
            this->value = value;

            if (this->isEnabled) {
                for (const auto & handler : onchange) {
                    (*handler)(value);
                }
            }
        }
    }

    void on(const std::function<void(const T &)> & handler) override {
        onchange.insert(&handler);
    }

    void off(const std::function<void(const T &)> & handler) override {
        onchange.erase(&handler);
    }


    void enable() override {
        if (!this->isEnabled) {
            for (const auto & handler : onchange) {
                (*handler)(value);
            }
            this->isEnabled = true;
        }
    }

    void disable() override {
        this->isEnabled = false;
    }

    // Destroyable interface

    ~Reference() {
        onchange.clear();
    }
};
