#pragma once

#include "reference.h++"

#include <emscripten.h>
#include <iostream>
#include <vasille/core/errors.h++>


template <typename T>
class Mirror : public Reference<T>
{
protected:
    IValue<T> * pointedValue;

    std::function<void(T)> handler;

public:
    bool forwardOnly;

    Mirror(IValue<T> * value, bool forwardOnly = false)
        : forwardOnly(forwardOnly)
        , Reference<T>(*value) {
        EM_ASM(
          console.log('constructor', $0, $1, $2), int(pointedValue),
          bool(handler), value == nullptr);
        std::cout << "Cout " << int(pointedValue) << ' ' << bool(handler);
        if (value == nullptr) {
            throw userError("Null Pointer Error", "null");
        }
        handler = [this](const T & value) { *this = value; };
    }



    // IValue interface
    void operator=(const T & value) override {
        if (!forwardOnly) {
            *pointedValue = value;
        }
        else {
            Reference<T>::operator=(value);
        }
    }

    void enable() override {
        if (!this->isEnabled) {
            this->isEnabled = true;
            pointedValue->on(handler);
            Reference<T>::operator=(*pointedValue);
        }
    }

    void disable() override {
        if (this->isEnabled) {
            EM_ASM(console.log($0, $1), int(pointedValue), bool(handler));
            EM_ASM({ console.log('I received: ' + $0); }, 100);
            //            pointedValue->off(handler);
            this->isEnabled = false;
        }
    }

    ~Mirror() {
        disable();
    }
};
