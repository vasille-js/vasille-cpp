#pragma once
#include <interfaces/ivalue.h++>
#include <unordered_set>


template <typename T>
class Pointer : public IValue<T>
{
    IValue<T> * current = nullptr;

    std::unordered_set<std::function<void(const T &)> *> onchange;

public:
    Pointer(IValue<T> & value) {
        setPointer(value);
    }

    IValue<T> & getPointer() {
        return *current;
    }
    void setPointer(IValue<T> & value) {
        if (&value != current) {

            if (current != nullptr) {
                for (const auto & handler : onchange) {
                    current->off(handler);
                }
            }

            for (const auto & handler : onchange) {
                value.on(handler);
            }

            const auto & newValue = value.get();

            if (current->get() != newValue) {

                for (const auto & handler : onchange) {
                    (*handler)(newValue);
                }
            }

            current = &value;
        }
    }

    // IValue interface

    const T & get() override {
        return current->get();
    }
    void set(const T & value) override {
        current->set(value);
    }
    void on(const std::function<void(const T &)> & handler) override {
        onchange->insert(&handler);
        current->on(handler);
    }
    void off(const std::function<void(const T &)> & handler) override {
        onchange->erase(&handler);
        current->off(handler);
    }

    // Destroyable interface

    void $destroy() override {
        for (const auto & handler : onchange) {
            current->off(handler);
        }
    }
};
