#pragma once
#include "mirror.h++"

template <typename T>
class Pointer : public Mirror<T>
{
public:
    Pointer(IValue<T> * value, bool forwardOnly = false)
        : Mirror<T>(value, forwardOnly) {}

    void point(IValue<T> * value) {
        if (this->pointedValue != value) {
            this->disable();
            this->pointedValue = value;
            this->enable();
        }
    }
};
