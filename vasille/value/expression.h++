#pragma once

#include "reference.h++"

#include <vasille/core/ivalue.h++>
#include <vector>



template <typename T, typename T1, typename... Args>
class VoidExpression : public Switchable
{
protected:
    std::tuple<IValue<T1> *, IValue<Args> *...> values;

    std::tuple<T1, Args...> valuesCache;

    std::function<T(const T1 &, const Args &...)> func;

    std::tuple<
      std::function<void(const T1 &)>, std::function<void(const Args &)>...>
      handlers;

    bool enabled;

protected:
    template <std::size_t... I>
    inline T process(std::index_sequence<I...>) {
        return func(std::get<I>(valuesCache)...);
    }

    T process() {
        return process(std::make_index_sequence<
                       std::tuple_size<std::tuple<T1, Args...>>::value>());
    }

    template <std::size_t I>
    inline void createHandlers() {
        std::get<I>(handlers) =
          [this](const typename std::tuple_element<
                 I, std::tuple<T1, Args...>>::type & value) {
              std::get<I>(valuesCache) = value;
              onUpdate();
          };
    }

    template <std::size_t I, std::size_t Next, std::size_t... Rest>
    inline void createHandlers() {
        createHandlers<I>();
        createHandlers<Next, Rest...>();
    }

    template <std::size_t... I>
    inline void createHandlers(std::index_sequence<I...>) {
        createHandlers<I...>();
    }

    void createHandlers() {
        createHandlers(std::make_index_sequence<
                       std::tuple_size<std::tuple<T1, Args...>>::value>());
    }

    template <std::size_t I>
    inline void updateAll() {
        std::get<I>(valuesCache) = *std::get<I>(values);
    }

    template <std::size_t I, std::size_t Next, std::size_t... Rest>
    inline void updateAll() {
        updateAll<I>();
        updateAll<Next, Rest...>();
    }

    template <std::size_t... I>
    inline void updateAll(std::index_sequence<I...>) {
        updateAll<I...>();
    }

    void updateAll() {
        updateAll(std::make_index_sequence<
                  std::tuple_size<std::tuple<T1, Args...>>::value>());
    }

    template <std::size_t I>
    inline void connectAll() {
        std::get<I>(values)->on(*std::get<I>(handlers));
    }

    template <std::size_t I, std::size_t Next, std::size_t... Rest>
    inline void connectAll() {
        connectAll<I>();
        connectAll<Next, Rest...>();
    }

    template <std::size_t... I>
    inline void connectAll(std::index_sequence<I...>) {
        connectAll<I...>();
    }

    void connectAll() {
        updateAll(std::make_index_sequence<
                  std::tuple_size<std::tuple<T1, Args...>>::value>());
    }

    template <std::size_t I>
    inline void disconnectAll() {
        std::get<I>(values)->off(*std::get<I>(handlers));
    }

    template <std::size_t I, std::size_t Next, std::size_t... Rest>
    inline void disconnectAll() {
        disconnectAll<I>();
        disconnectAll<Next, Rest...>();
    }

    template <std::size_t... I>
    inline void disconnectAll(std::index_sequence<I...>) {
        disconnectAll<I...>();
    }

    void disconnectAll() {
        updateAll(std::make_index_sequence<
                  std::tuple_size<std::tuple<T1, Args...>>::value>());
    }

    virtual void onUpdate() {
        process();
    }


public:
    VoidExpression(
      std::function<T(const T1 &, const Args &...)> func, bool link,
      IValue<T1> * first, IValue<Args> *... input)
        : func(func) {

        values = std::make_tuple(first, input...);

        createHandlers();

        if (link) {
            enable();
        }
        else {
            updateAll();
            onUpdate();
        }
    }

    // IBind interface

    void enable() override {
        if (!enabled) {
            updateAll();
            connectAll();
            onUpdate();
            enabled = true;
        }
    }

    void disable() override {
        if (enabled) {
            disconnectAll();
            enabled = false;
        }
    }

    // Destroyable interface

    ~VoidExpression() {
        disable();
    }
};

template <typename T, typename T1, typename... Args>
class Expression
    : public VoidExpression<T, T1, Args...>
    , public IValue<T>
{
    Reference<T> sync;

protected:
    void onUpdate() override {
        // trigger value update (if updated)
        sync = VoidExpression<T, T1, Args...>::process();
    }

public:
    Expression(
      std::function<T(const T1 &, const Args &...)> func, bool link,
      IValue<T1> * first, IValue<Args> *... input)
        : VoidExpression<T, T1, Args...>(func, link, first, input...)
        , IValue<T>(link) {
        sync = VoidExpression<T, T1, Args...>::process();
    }

    // IValue interface

    operator const T &() override {
        return sync;
    }

    void operator=(const T & value) override {
        sync = value;
    }

    void on(const std::function<void(const T &)> & handler) override {
        sync.on(handler);
    }

    void off(const std::function<void(const T &)> & handler) override {
        sync.off(handler);
    }


    // Switchable interface
public:
    void enable() override {
        VoidExpression<T, T1, Args...>::enable();
        this->isEnabled = true;
    }
    void disable() override {
        VoidExpression<T, T1, Args...>::disable();
        this->isEnabled = false;
    }
};
