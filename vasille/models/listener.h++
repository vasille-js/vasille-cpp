#pragma once

#include <functional>
#include <unordered_set>

template <typename ValueT, typename IndexT>
class Listener
{
    using Handler = std::function<void(const IndexT &, const ValueT &)>;

    std::unordered_set<Handler *> onAdded;
    std::unordered_set<Handler *> onRemoved;

public:
    void emitAdded(const IndexT & index, const ValueT & value) {
        for (auto handler : onAdded) {
            (*handler)(index, value);
        }
    }

    void emitRemoved(const IndexT & index, const ValueT & value) {
        for (auto handler : onRemoved) {
            (*handler)(index, value);
        }
    }

    void onAdd(Handler * handler) {
        onAdded.insert(handler);
    }

    void onRemove(Handler * handler) {
        onRemoved.insert(handler);
    }

    void offAdd(Handler * handler) {
        auto it = onAdded.find(handler);

        if (it != onAdded.end()) {
            onAdded.erase(it);
        }
    }

    void offRemove(Handler * handler) {
        auto it = onRemoved.find(handler);

        if (it != onRemoved.end()) {
            onRemoved.erase(it);
        }
    }

    void enableReactivity() {
        //
    }

    void disableReactivity() {
        //
    }
};
