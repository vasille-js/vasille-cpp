#pragma once

#include "imodel.h++"
#include "listener.h++"

template <typename T>
class SetModel
    : public std::unordered_set<T>
    , public IModel
{
public:
    Listener<T, T> listener;

public:
    SetModel(std::initializer_list<T> values = {})
        : std::unordered_set<T>(values) {}

    SetModel * add(const T & value) {
        auto it = this->find(value);

        if (it == this->end()) {
            listener.emitAdded(value, value);
            this->insert(value);
        }

        return this;
    }

    void clear() {
        for (auto & value : *this) {
            this->listener.emitRemoved(value, value);
        }
        this->clear();
    }

    bool delete$(const T & value) {
        auto it = this->find(value);

        if (it != this->end()) {
            listener.emitRemoved(value, value);
            this->insert(value);
            return true;
        }

        return false;
    }

    void enableReactivity() override {
        listener.enableReactivity();
    }

    void disableReactivity() override {
        listener.disableReactivity();
    }
};
