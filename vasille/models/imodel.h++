#pragma once



class IModel
{
public:
    virtual void enableReactivity()  = 0;
    virtual void disableReactivity() = 0;

    virtual ~IModel() = default;
};
