#pragma once
#include "destroyable.h++"

#include <functional>

class Switchable : public Destroyable
{
public:
    virtual void enable()  = 0;
    virtual void disable() = 0;
};

template <typename T>
class IValue : public Switchable
{
protected:
    bool isEnabled;

public:
    IValue(bool isEnabled)
        : isEnabled(isEnabled) {}

    virtual      operator const T &() = 0;
    virtual void operator=(const T & value) = 0;

    virtual void on(const std::function<void(const T &)> & handler)  = 0;
    virtual void off(const std::function<void(const T &)> & handler) = 0;

    ~IValue() = default;
};
