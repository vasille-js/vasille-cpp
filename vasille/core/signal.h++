#pragma once
#include <functional>
#include <unordered_set>



template <typename... Args>
class Signal
{
    std::unordered_set<std::function<void(Args...)>> handlers;

    void operator()(Args... args) const {
        for (auto & it : handlers) {
            it(args...);
        }
    }

    void subscribe(std::function<void(Args...)> func) {
        handlers.insert(func);
    }

    void unsubscribe(std::function<void(Args...)> func) {
        handlers.erase(func);
    }
};
