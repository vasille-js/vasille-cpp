#pragma once

class Destroyable
{
public:
    virtual ~Destroyable() = default;
};
