#pragma once
#include <string>

std::string notOverwritten();

std::string internalError(const std::string & msg);

std::string userError(const std::string & msg, const std::string & err);

std::string wrongBinding(const std::string & msg);
