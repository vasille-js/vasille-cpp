#pragma once
#include "emscripten/val.h"
#include <string>

class Executor
{
public:
    virtual void addClass(emscripten::val & el, const std::string & cl)    = 0;
    virtual void removeClass(emscripten::val & el, const std::string & cl) = 0;
    virtual void setAttribute(
      emscripten::val & el, const std::string & name,
      const std::string & value) = 0;
    virtual void removeAttribute(
      emscripten::val & el, const std::string & name) = 0;
    virtual void setStyle(
      emscripten::val & el, const std::string & prop,
      const std::string & value) = 0;
    virtual void insertBefore(
      emscripten::val & el, emscripten::val & child,
      emscripten::val & before)                                             = 0;
    virtual void appendChild(emscripten::val & el, emscripten::val & child) = 0;
    virtual void callCallback(const std::function<void()> & cb)             = 0;
};

class InstantExecutor : public Executor
{
    // Executor interface
public:
    void addClass(emscripten::val & el, const std::string & cl) override;
    void removeClass(emscripten::val & el, const std::string & cl) override;
    void setAttribute(
      emscripten::val & el, const std::string & name,
      const std::string & value) override;
    void removeAttribute(
      emscripten::val & el, const std::string & name) override;
    void setStyle(
      emscripten::val & el, const std::string & prop,
      const std::string & value) override;
    void insertBefore(
      emscripten::val & el, emscripten::val & child,
      emscripten::val & before) override;
    void appendChild(emscripten::val & el, emscripten::val & child) override;
    void callCallback(const std::function<void()> & cb) override;

    static InstantExecutor * instance();
};

class TimeoutExecutor : public InstantExecutor
{

    // Executor interface
public:
    void addClass(emscripten::val & el, const std::string & cl) override;
    void removeClass(emscripten::val & el, const std::string & cl) override;
    void setAttribute(
      emscripten::val & el, const std::string & name,
      const std::string & value) override;
    void removeAttribute(
      emscripten::val & el, const std::string & name) override;
    void setStyle(
      emscripten::val & el, const std::string & prop,
      const std::string & value) override;
    void insertBefore(
      emscripten::val & el, emscripten::val & child,
      emscripten::val & before) override;
    void appendChild(emscripten::val & el, emscripten::val & child) override;
    void callCallback(const std::function<void()> & cb) override;

    static TimeoutExecutor * instance();
};
