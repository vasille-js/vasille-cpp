#pragma once
#include "ivalue.h++"

#include <unordered_map>
#include <unordered_set>
#include <vasille/models/imodel.h++>
#include <vasille/value/expression.h++>
#include <vasille/value/mirror.h++>
#include <vasille/value/pointer.h++>



class App;



class ReactivePrivate : public Destroyable
{
public:
    ReactivePrivate() = default;

protected:
    std::unordered_set<Switchable *>  watch;
    std::unordered_set<Destroyable *> bindings;
    std::unordered_set<IModel *>      models;

    bool enabled = true;
    bool frozen  = false;

    VoidExpression<void, bool> * freezeExpr = nullptr;

public:
    friend class Reactive;

    ~ReactivePrivate();
};

class Reactive : public Destroyable
{
protected:
    ReactivePrivate * $;

public:
    Reactive(ReactivePrivate * $ = new ReactivePrivate);
    ~Reactive();

    template <typename T>
    IValue<T> * ref(T value) {
        auto ref = new Reference<T>(value);
        $->watch.insert(ref);
        return ref;
    }

    template <typename T>
    IValue<T> * mirror(IValue<T> * value) {
        auto mirror = new Mirror<T>(value, false);

        $->watch.insert(mirror);
        return mirror;
    }

    template <typename T>
    IValue<T> * forward(IValue<T> * value) {
        auto mirror = new Mirror<T>(value, true);

        $->watch.insert(mirror);
        return mirror;
    }

    template <typename T>
    IValue<T> * point(IValue<T> * value, bool forwardOnly = false) {
        auto pointer = new Pointer<T>(value, forwardOnly);

        $->watch.insert(pointer);
        return pointer;
    }

    template <typename T>
    T * register$(T * model) {
        $->models.insert(model);
        return model;
    }

    template <typename... Args>
    void watch(
      std::function<void(const Args &...)> func, IValue<Args> *... input) {
        $->watch.insert(new VoidExpression<void, Args...>(
          std::move(func), !$->frozen, input...));
    }

    template <typename T, typename... Args>
    IValue<T> * bind(
      std::function<T(const Args &...)> func, IValue<Args> *... input) {
        auto expr =
          new Expression<T, Args...>(std::move(func), !$->frozen, input...);
        $->watch.insert(static_cast<VoidExpression<T, Args...> *>(expr));
        return expr;
    }

    void enable();

    void disable();

    void bindAlive(
      IValue<bool> * cond, std::function<void()> onOff = nullptr,
      std::function<void()> onOn = nullptr);
};
