#include <functional>

#pragma once

template <class T, typename... Args>
class Slot
{
    std::function<void(T, Args...)> runner;

public:
    void operator()(std::function<void(T, Args...)> func) {
        runner = func;
    }

    void operator()(
      std::function<void(T, Args...)> func, T node, Args... args) {
        if (runner) {
            runner(node, args...);
        }
        else {
            func(node, args...);
        }
    }

    void operator()(T node, Args... args) {
        if (runner) {
            runner(node, args...);
        }
    }
};
