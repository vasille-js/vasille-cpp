#pragma once

#include <emscripten/html5.h>
#include <emscripten/val.h>
#include <map>
#include <memory>
#include <vasille/binding/attribute.h++>
#include <vasille/binding/style.h++>
#include <vasille/core/core.h++>
#include <vector>



class AppNode;
class Fragment;
class TextNode;
class Tag;
class SwitchedNode;

struct Case
{
    IValue<bool> * cond;

    std::function<void(Fragment *)> cb;
};

class FragmentPrivate : public ReactivePrivate
{
protected:
    AppNode *  app    = nullptr;
    Fragment * parent = nullptr;
    Fragment * next   = nullptr;
    Fragment * prev   = nullptr;

public:
    void preinit(AppNode * app, Fragment * parent);

    ~FragmentPrivate();

    friend class Fragment;
};

class Fragment : public Reactive
{
protected:
    FragmentPrivate * $;

public:
    std::vector<Fragment *> children;

    Fragment(FragmentPrivate * $ = new FragmentPrivate);

    ~Fragment();

    inline AppNode * app() {
        return $->app;
    }

    void preinit(AppNode * app, Fragment * parent);

    virtual void created();

    virtual void mounted();

    virtual void ready();

    virtual void createWatchers();

    virtual void compose();

    void init();

    void text(const std::string & text, const std::function<void(TextNode)>);
    void text(IValue<std::string> * text, const std::function<void(TextNode)>);

    void debug(IValue<std::string> * text);

    void tag(
      const std::string &                                   tagName,
      const std::function<void(Tag *, emscripten::val &)> & cb);

    template <class T>
    void create(
      T * node, std::function<void(T *)> callback = nullptr,
      std::function<void(T *)> callback2 = nullptr) {
        static_cast<Fragment *>(node)->preinit($->app, this);

        if (callback) {
            callback(node);
        }
        if (callback2) {
            callback2(node);
        }

        pushNode(node);
        node->init();
        node->ready();
    }

    void if$(IValue<bool> * cond, std::function<void(Fragment *)> cb);
    void if_else(
      IValue<bool> * cond, std::function<void(Fragment *)> ifCb,
      std::function<void(Fragment *)> elseCb);

    void switch$(Case cases...);
    Case case$(IValue<bool> * cond, std::function<void(Fragment *)> cb);
    Case default$(std::function<void(Fragment *)> cb);

private:
    inline void add_case(SwitchedNode * node, Case first, Case next...);
    inline void add_case(SwitchedNode * node, Case first);

protected:
    void pushNode(Fragment * node);

    virtual emscripten::val * findFirstChild();

    virtual void appendNode(emscripten::val & node);

    virtual void insertAdjacent(emscripten::val & node);

public:
    void insertBefore(Fragment * node);
    void insertAfter(Fragment * node);
    void remove();

    friend class Tag;
    friend class DebugPrivate;
};

class TextNodePrivate : public FragmentPrivate
{
protected:
    emscripten::val node = emscripten::val::undefined();

    void preinitText(
      AppNode * app, Fragment * parent, IValue<std::string> * text);

public:
    friend class TextNode;
};


class TextNode : public Fragment
{
    emscripten::val   node = emscripten::val::undefined();
    TextNodePrivate * $;

public:
    TextNode(TextNodePrivate * $ = new TextNodePrivate);

    ~TextNode();

    void preinit(AppNode * app, Fragment * parent, IValue<std::string> * text);

    // Fragment interface
protected:
    emscripten::val * findFirstChild() override;
};

class INodePrivate : public FragmentPrivate
{
protected:
    bool unmounted = false;

    emscripten::val node = emscripten::val::undefined();

public:
    friend class INode;
};

class INode : public Fragment
{
    INodePrivate * $;

    std::vector<std::function<void()> *> handlers;

public:
    INode(INodePrivate * $ = new INodePrivate);
    ~INode();

    emscripten::val & node() {
        return $->node;
    }

    void init() {
        createWatchers();
        createAttrs();
        createStyle();

        created();
        compose();
        mounted();
    }

    virtual void createAttrs() {}

    virtual void createStyle() {}

    void attr(const std::string & name, IValue<std::string> * value) {
        $->bindings.insert(new AttributeBinding(this, name, value));
    }

    template <typename T1, typename... Args>
    void bindAttr(
      const std::string &                                           name,
      const std::function<std::string(const T1 &, const Args &...)> calculator,
      IValue<T1> & v1, IValue<Args> &... vn) {

        $->bindings.insert(
          new AttributeBinding(this, name, this->bind(calculator, v1, vn...)));
    }

    void setAttr(const std::string & name, const std::string & value) {
        //     $->app->$run->setAttribute();
    }

    void addClass(const std::string & cl);

    void addClasses(const std::string & cl);

    template <typename... Rest>
    void addClasses(const std::string & cl, Rest... rest) {
        addClasses(cl);
        addClasses(rest...);
    }

    void bindClass(IValue<std::string> * cl);

    void bindClass(IValue<bool> * bind, const std::string & cl);

    void style(const std::string & name, IValue<std::string> * value) {
        auto $ = dynamic_cast<INodePrivate *>(this->$);
        $->bindings.insert(new StyleBinding(this, name, value));
    }

    template <typename T1, typename... Args>
    void bindStyle(
      const std::string &                                           name,
      const std::function<std::string(const T1 &, const Args &...)> calculator,
      IValue<T1> * v1, IValue<Args> *... vn) {
        auto $ = dynamic_cast<INodePrivate *>(this->$);
        $->bindings.insert(
          new StyleBinding(this, name, this->bind(calculator, v1, vn...)));
    }

    void setStyle(const std::string & name, const std::string & value) {
        //     $->app->$run->
    }

private:
    std::string id();

    template <typename T>
    std::function<bool(const T *)> * handle(
      const std::function<bool(const T *)> & func) {
        auto handler = new std::function(func);

        handlers.push_back(reinterpret_cast<std::function<void()> *>(handler));
        return handler;
    }

public:
    using ME = std::function<bool(const EmscriptenMouseEvent *)>;
    using FE = std::function<bool(const EmscriptenFocusEvent *)>;
    using KE = std::function<bool(const EmscriptenKeyboardEvent *)>;
    using TE = std::function<bool(const EmscriptenTouchEvent *)>;
    using WE = std::function<bool(const EmscriptenWheelEvent *)>;
    // using PrE = std::function<bool(const EmscriptenProgressEvent *)>;
    // using DE  = std::function<bool(const EmscriptenDragEvent *)>;
    // using PoE = std::function<bool(const EmscriptenPointerEvent *)>;
    // using AE  = std::function<bool(const EmscriptenAnimationEvent *)>;
    // using CE  = std::function<bool(const EmscriptenClipboardEvent *)>;

    struct EventListenerOptions
    {
        bool capture = false;
        bool once    = false;
        bool passive = false;
    };

    // void oncontextmenu(ME handler, bool useCapture = false);
    // void oncontextmenu(ME handler, const EventListenerOptions & options);

    void onmousedown(ME handler, bool useCapture = false);
    // void onmousedown(ME handler, const EventListenerOptions & options);

    void onmouseenter(ME handler, bool useCapture = false);
    // void onmouseenter(ME handler, const EventListenerOptions & options);

    void onmouseleave(ME handler, bool useCapture = false);
    // void onmouseleave(ME handler, const EventListenerOptions & options);

    void onmousemove(ME handler, bool useCapture = false);
    // void onmousemove(ME handler, const EventListenerOptions & options);

    void onmouseout(ME handler, bool useCapture = false);
    // void onmouseout(ME handler, const EventListenerOptions & options);

    void onmouseover(ME handler, bool useCapture = false);
    // void onmouseover(ME handler, const EventListenerOptions & options);

    void onmouseup(ME handler, bool useCapture = false);
    // void onmouseup(ME handler, const EventListenerOptions & options);

    void onclick(ME handler, bool useCapture = false);
    // void onclick(ME handler, const EventListenerOptions & options);

    void ondblclick(ME handler, bool useCapture = false);
    // void ondblclick(ME handler, const EventListenerOptions & options);

    void onblur(FE handler, bool useCapture = false);
    // void onblur(FE handler, const EventListenerOptions & options);

    void onfocus(FE handler, bool useCapture = false);
    // void onfocus(FE handler, const EventListenerOptions & options);

    void onfocusin(FE handler, bool useCapture = false);
    // void onfocusin(FE handler, const EventListenerOptions & options);

    void onfocusout(FE handler, bool useCapture = false);
    // void onfocusout(FE handler, const EventListenerOptions & options);

    void onkeydown(KE handler, bool useCapture = false);
    // void onkeydown(KE handler, const EventListenerOptions & options);

    void onkeyup(KE handler, bool useCapture = false);
    // void onkeyup(KE handler, const EventListenerOptions & options);

    void onkeypress(KE handler, bool useCapture = false);
    // void onkeypress(KE handler, const EventListenerOptions & options);

    void ontouchstart(TE handler, bool useCapture = false);
    // void ontouchstart(TE handler, const EventListenerOptions & options);

    void ontouchmove(TE handler, bool useCapture = false);
    // void ontouchmove(TE handler, const EventListenerOptions & options);

    void ontouchend(TE handler, bool useCapture = false);
    // void ontouchend(TE handler, const EventListenerOptions & options);

    void ontouchcancel(TE handler, bool useCapture = false);
    // void ontouchcancel(TE handler, const EventListenerOptions & options);

    void onwheel(WE handler, bool useCapture = false);
    // void onwheel(WE handler, const EventListenerOptions & options);

    // void onabort(PrE handler, bool useCapture = false);
    // void onabort(PrE handler, const EventListenerOptions & options);

    // void onerror(PrE handler, bool useCapture = false);
    // void onerror(PrE handler, const EventListenerOptions & options);

    // void onload(PrE handler, bool useCapture = false);
    // void onload(PrE handler, const EventListenerOptions & options);

    // void onloadend(PrE handler, bool useCapture = false);
    // void onloadend(PrE handler, const EventListenerOptions & options);

    // void onloadstart(PrE handler, bool useCapture = false);
    // void onloadstart(PrE handler, const EventListenerOptions & options);

    // void onprogress(PrE handler, bool useCapture = false);
    // void onprogress(PrE handler, const EventListenerOptions & options);

    // void ontimeout(PrE handler, bool useCapture = false);
    // void ontimeout(PrE handler, const EventListenerOptions & options);

    // void ondrag(DE handler, bool useCapture = false);
    // void ondrag(DE handler, const EventListenerOptions & options);

    // void ondragend(DE handler, bool useCapture = false);
    // void ondragend(DE handler, const EventListenerOptions & options);

    // void ondragenter(DE handler, bool useCapture = false);
    // void ondragenter(DE handler, const EventListenerOptions & options);

    // void ondragexit(DE handler, bool useCapture = false);
    // void ondragexit(DE handler, const EventListenerOptions & options);

    // void ondragleave(DE handler, bool useCapture = false);
    // void ondragleave(DE handler, const EventListenerOptions & options);

    // void ondragover(DE handler, bool useCapture = false);
    // void ondragover(DE handler, const EventListenerOptions & options);

    // void ondragstart(DE handler, bool useCapture = false);
    // void ondragstart(DE handler, const EventListenerOptions & options);

    // void ondrop(DE handler, bool useCapture = false);
    // void ondrop(DE handler, const EventListenerOptions & options);

    // void onpointerover(PoE handler, bool useCapture = false);
    // void onpointerover(PoE handler, const EventListenerOptions & options);

    // void onpointerenter(PoE handler, bool useCapture = false);
    // void onpointerenter(PoE handler, const EventListenerOptions &
    // options);

    // void onpointerdown(PoE handler, bool useCapture = false);
    // void onpointerdown(PoE handler, const EventListenerOptions & options);

    // void onpointermove(PoE handler, bool useCapture = false);
    // void onpointermove(PoE handler, const EventListenerOptions & options);

    // void onpointerup(PoE handler, bool useCapture = false);
    // void onpointerup(PoE handler, const EventListenerOptions & options);

    // void onpointercancel(PoE handler, bool useCapture = false);
    // void onpointercancel(PoE handler, const EventListenerOptions &
    // options);

    // void onpointerout(PoE handler, bool useCapture = false);
    // void onpointerout(PoE handler, const EventListenerOptions & options);

    // void onpointerleave(PoE handler, bool useCapture = false);
    // void onpointerleave(PoE handler, const EventListenerOptions &
    // options);

    // void ongotpointercapture(PoE handler, bool useCapture = false);
    // void ongotpointercapture(PoE handler, const EventListenerOptions &
    // options);

    // void onlostpointercapture(PoE handler, bool useCapture = false);
    // void onlostpointercapture(
    //   PoE handler, const EventListenerOptions & options);

    // void onanimationstart(AE handler, bool useCapture = false);
    // void onanimationstart(AE handler, const EventListenerOptions &
    // options);

    // void onanimationend(AE handler, bool useCapture = false);
    // void onanimationend(AE handler, const EventListenerOptions & options);

    // void onanimationiteraton(AE handler, bool useCapture = false);
    // void onanimationiteraton(AE handler, const EventListenerOptions &
    // options);

    // void onclipboardchange(CE handler, bool useCapture = false);
    // void onclipboardchange(CE handler, const EventListenerOptions &
    // options);

    // void oncut(CE handler, bool useCapture = false);
    // void oncut(CE handler, const EventListenerOptions & options);

    // void oncopy(CE handler, bool useCapture = false);
    // void oncopy(CE handler, const EventListenerOptions & options);

    // void onpaste(CE handler, bool useCapture = false);
    // void onpaste(CE handler, const EventListenerOptions & options);

    void insertAdjacent(emscripten::val & node) {
        // $.app.$run.insertBefore($.node, node);
    }

    void bindShow(IValue<bool> * value);

    void html(IValue<std::string> * value);
};

class TagPrivate : public INodePrivate
{
public:
    friend class Tag;
};

class Tag : public INode
{
    TagPrivate * $;

public:
    Tag(TagPrivate * $ = new TagPrivate);
    ~Tag();

    void preinit(AppNode * app, Fragment * parent, const std::string & tagName);

    void bindMount(IValue<bool> * cond);

    // Fragment interface
protected:
    emscripten::val * findFirstChild() override;

    void appendNode(emscripten::val & node) override {
        // $.app.$run.appendChild($.node, node);
    }

    void insertAdjacent(emscripten::val & node) override;
};

class ExtensionPrivate : public INodePrivate
{
public:
    friend class Extension;
};

class Extension : public INode
{
    ExtensionPrivate * $;

public:
    Extension(ExtensionPrivate * $ = new ExtensionPrivate);

    void preinit(AppNode * app, Fragment * parent0);
};

class ComponentPrivate : public ExtensionPrivate
{
public:
    friend class Component;
};

class Component : public Extension
{
    ComponentPrivate * $;

public:
    Component(ComponentPrivate * $ = new ComponentPrivate);

    void preinit(AppNode * app, Fragment * parent);

    // Fragment interface
public:
    void mounted() override;
};

class SwitchedNodePrivate : public FragmentPrivate
{
protected:
    int index = -1;

    Fragment * fragment = nullptr;

    std::vector<Case> cases;

    std::function<void(const bool &)> sync;

public:
    friend class SwitchedNode;
};

class SwitchedNode : public Fragment
{
    SwitchedNodePrivate * $;

public:
    SwitchedNode(SwitchedNodePrivate * $);

    ~SwitchedNode();

    void addCase(const Case & case$);

    void createChild(std::function<void(Fragment *)> cb);

    void ready() override;
};

class DebugPrivate : public FragmentPrivate
{
protected:
    emscripten::val node = emscripten::val::undefined();

public:
    ~DebugPrivate();

    void preinit(AppNode * app, Fragment * parent, IValue<std::string> * text);
};

class DebugNode : public Fragment
{
    DebugPrivate * $;

public:
    void preinit(AppNode * app, Fragment * parent, IValue<std::string> * text) {
        $->preinit(app, parent, text);
    }
};

/*
 * *
 * *
 * *
 * *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
