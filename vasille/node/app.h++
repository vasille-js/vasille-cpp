#pragma once

#include "node.h++"

#include <vasille/core/executor.h++>
#include <vasille/node/node.h++>

struct AppOptions
{
    bool       debugUi  = false;
    bool       freezeUi = true;
    Executor * executor = nullptr;
};

class AppNodePrivate : public INodePrivate
{
public:
    friend class AppNode;
    friend class App;
};

class AppNode : public INode
{
protected:
    AppNodePrivate * $;

public:
    Executor * run;

    bool debugUi;

    AppNode(
      const AppOptions & options, AppNodePrivate * $ = new AppNodePrivate);
};

class App : public AppNode
{
public:
    App(const emscripten::val & node, const AppOptions & options);

    void appendNode(emscripten::val & node) override;
};
