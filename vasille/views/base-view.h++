#pragma once

#include "repeat-node.h++"

template <typename K, typename T>
class BaseViewPrivate : public RepeatNodePrivate<K>
{
public:
    std::function<void(const K &, const T &)> addHandler;
    std::function<void(const K &, const T &)> removeHandler;
};

template <typename K, typename T, typename Model>
class BaseView : public RepeatNode<K, T>
{
    BaseViewPrivate<K, T> * $;

public:
    Model * model;

    BaseView(BaseViewPrivate<K, T> * $ = new BaseViewPrivate<K, T>)
        : RepeatNode<K, T>($)
        , $($) {
        $->addHandler = [this](const K & id, const T & item) {
            RepeatNode<K, T>::createChild(id, item);
        };
        $->removeHandler = [this](const K & id, const T & item) {
            RepeatNode<K, T>::destroyChild(id, item);
        };
    }

    ~BaseView() {
        model->listener.offAdd(&$->addHandler);
        model->listener.offRemove(&$->removeHandler);
    }

    void ready() override {
        model->listener.onAdd(&$->addHandler);
        model->listener.onRemove(&$->removeHandler);
    }
};
