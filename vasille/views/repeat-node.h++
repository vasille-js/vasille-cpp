#pragma once

#include <vasille/core/slot.h++>
#include <vasille/node/app.h++>
#include <vasille/node/node.h++>

template <typename IdT>
class RepeatNodePrivate : public INodePrivate
{
public:
    std::unordered_map<IdT, Fragment *> nodes;
};

template <typename IdT, typename T>
class RepeatNode : public Fragment
{
    RepeatNodePrivate<IdT> * $;

public:
    Slot<Fragment *, T, IdT> slot;

    bool freezeUi = true;

    RepeatNode(RepeatNodePrivate<IdT> * $ = new RepeatNodePrivate<IdT>)
        : Fragment($)
        , $($) {}

    void createChild(
      const IdT & id, const T & item, Fragment * before = nullptr) {
        auto node = new Fragment();

        destroyChild(id, item);

        if (before) {
            children.insert(
              std::find(children.begin(), children.end(), before), node);
            before->insertBefore(node);
        }
        else {
            auto last = children.rbegin();

            if (last != children.rend()) {
                (*last)->insertAfter(node);
            }

            children.push_back(node);
        }

        node->preinit(app(), this);
        node->init();

        auto callback = [&]() {
            slot(node, item, id);
            node->ready();
        };

        if (freezeUi) {
            app()->run->callCallback(callback);
        }
        else {
            TimeoutExecutor::instance()->callCallback(callback);
        }
    }

    void destroyChild(const IdT & id, const T &) {
        auto child = $->nodes.find(id);

        if (child != $->nodes.end()) {
            std::get<Fragment *>(*child)->remove();
            children.erase(std::find(
              children.begin(), children.end(), std::get<Fragment *>(*child)));
            $->nodes.erase(child);
        }
    }
};
