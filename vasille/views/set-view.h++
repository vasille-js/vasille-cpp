#pragma once

#include "base-view.h++"

#include <vasille/models/set-model.h++>

template <typename T>
class SetView : public BaseView<T, T, SetModel<T>>
{
public:
    SetView(SetModel<T> * model) {
        this->model = model;
    }

    void ready() override {
        for (auto & item : *this->model) {
            this->app()->run->callCallback(
              [=]() { this->createChild(item, item); });
            return;
        }
    }
};
