#pragma once

#include "binding.h++"

#include <string>

class INode;

void addClass(INode * node, const std::string & className);

void removeClass(INode * node, const std::string & className);

class StaticClassBinding : public Binding<bool>
{
    bool current = false;

public:
    StaticClassBinding(
      INode * node, const std::string & name, IValue<bool> * value);
};

class DynamicClassBinding : public Binding<std::string>
{
    std::string current;

public:
    DynamicClassBinding(INode * node, IValue<std::string> * value);
    ;
};
