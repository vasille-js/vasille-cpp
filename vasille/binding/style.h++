#pragma once

#include "binding.h++"

class StyleBinding : public Binding<std::string>
{
public:
    StyleBinding(
      INode * node, const std::string & name, IValue<std::string> * value);
};
