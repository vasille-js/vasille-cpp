#pragma once

#include <string>
#include <tuple>
#include <vasille/core/destroyable.h++>
#include <vasille/core/ivalue.h++>
#include <vasille/value/expression.h++>
#include <vector>

class INode;


template <typename T, typename... Args>
class Binding : public Destroyable
{
    IValue<T> *                    binding = nullptr;
    std::function<void(const T &)> func;

protected:
    void init(std::function<void(const T &)> bounded) {
        func = bounded;

        binding->on(func);
        func(*binding);
    }

public:
    Binding(IValue<T> * value) {
        binding = value;
    }

    ~Binding() {
        binding->off(func);
    }
};
