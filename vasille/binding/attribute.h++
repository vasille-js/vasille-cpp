#pragma once

#include "binding.h++"


class AttributeBinding : public Binding<std::string>
{
public:
    AttributeBinding(
      INode * node, const std::string & name, IValue<std::string> * value);
};
