#include <binding/attribute.h++>
#include <node/app.h++>
#include <node/node.h++>

AttributeBinding::AttributeBinding(
  INode * node, const std::string & name, IValue<std::string> * value)
    : Binding<std::string>(value) {
    init([node, name](const std::string & value) {
        if (!value.empty()) {
            node->app()->run->setAttribute(node->node(), name, value);
        }
        else {
            node->app()->run->removeAttribute(node->node(), name);
        }
    });
}
