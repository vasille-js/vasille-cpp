#include <binding/class.h++>
#include <node/app.h++>
#include <node/node.h++>

void addClass(INode * node, const std::string & className) {
    node->app()->run->addClass(node->node(), className);
}

void removeClass(INode * node, const std::string & className) {
    node->app()->run->removeClass(node->node(), className);
}

StaticClassBinding::StaticClassBinding(
  INode * node, const std::string & name, IValue<bool> * value)
    : Binding<bool>(value) {
    init([this, node, name](bool value) {
        if (value != current) {
            if (value) {
                addClass(node, name);
            }
            else {
                removeClass(node, name);
            }
            current = value;
        }
    });
}

DynamicClassBinding::DynamicClassBinding(
  INode * node, IValue<std::string> * value)
    : Binding<std::string>(value) {
    init([this, node](const std::string & className) {
        if (current != className) {
            if (!current.empty()) {
                removeClass(node, current);
            }
            addClass(node, className);
            current = className;
        }
    });
}
