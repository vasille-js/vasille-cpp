#include <binding/style.h++>
#include <node/app.h++>
#include <node/node.h++>

StyleBinding::StyleBinding(
  INode * node, const std::string & name, IValue<std::string> * value)
    : Binding<std::string>(value) {
    init([this, node, name](const std::string & value) {
        node->app()->run->setStyle(node->node(), name, value);
    });
}
