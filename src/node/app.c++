#include <node/app.h++>

AppNode::AppNode(const AppOptions & options, AppNodePrivate * $)
    : INode($)
    , $($) {
    run = options.executor   ? options.executor
          : options.freezeUi ? InstantExecutor::instance()
                             : TimeoutExecutor::instance();

    debugUi = options.debugUi;
}

App::App(const emscripten::val & node, const AppOptions & options)
    : AppNode(options) {
    $->node = node;
    preinit(this, this);
}

void App::appendNode(emscripten::val & node) {
    run->appendChild($->node, node);
}
