#include <binding/attribute.h++>
#include <binding/class.h++>
#include <binding/style.h++>
#include <core/errors.h++>
#include <node/app.h++>
#include <node/node.h++>
#include <value/expression.h++>

const auto trueIValue = new Reference<bool>(true);

void FragmentPrivate::preinit(AppNode * app, Fragment * parent) {
    this->app    = app;
    this->parent = parent;
}

FragmentPrivate::~FragmentPrivate() {
    this->prev = nullptr;
    this->next = nullptr;
}

Fragment::Fragment(FragmentPrivate * $)
    : Reactive($)
    , $($) {}

Fragment::~Fragment() {
    for (auto child : children) {
        delete child;
    }
}

void Fragment::preinit(AppNode * app, Fragment * parent) {
    $->preinit(app, parent);
}

void Fragment::created() {}

void Fragment::mounted() {}

void Fragment::ready() {}

void Fragment::createWatchers() {}

void Fragment::compose() {}

void Fragment::init() {
    createWatchers();

    created();
    compose();
    mounted();
    ready();
}

void Fragment::tag(
  const std::string &                                   tagName,
  const std::function<void(Tag *, emscripten::val &)> & cb) {
    auto node = new Tag();

    node->preinit($->app, this, tagName);
    node->init();
    pushNode(node);

    $->app->run->callCallback([=]() {
        if (cb) {
            cb(node, node->node());
        }
        node->ready();
    });
}

Case Fragment::case$(IValue<bool> * cond, std::function<void(Fragment *)> cb) {
    return {.cond = cond, .cb = cb};
}

Case Fragment::default$(std::function<void(Fragment *)> cb) {
    return {.cond = trueIValue, .cb = cb};
}

void Fragment::pushNode(Fragment * node) {
    Fragment * lastChild = nullptr;

    if (!children.empty()) {
        lastChild = *children.end();
    }

    if (lastChild) {
        lastChild->$->next = node;
    }
    node->$->prev = lastChild;
    children.push_back(node);
}

emscripten::val * Fragment::findFirstChild() {
    emscripten::val * first = nullptr;

    for (auto & child : children) {
        if (!first) {
            first = child->findFirstChild();
        }
    }

    return first;
}

void Fragment::appendNode(emscripten::val & node) {
    if ($->next) {
        $->next->insertAdjacent(node);
    }
    else {
        $->parent->appendNode(node);
    }
}

void Fragment::insertAdjacent(emscripten::val & node) {
    auto first = findFirstChild();

    if (first) {
        // $->app.
    }
    else if ($->next) {
        $->next->insertAdjacent(node);
    }
    else {
        $->parent->appendNode(node);
    }
}

void Fragment::insertBefore(Fragment * node) {
    node->$->prev = $->prev;
    node->$->next = this;

    if ($->prev) {
        $->prev->$->next = node;
    }
    $->prev = node;
}

void Fragment::insertAfter(Fragment * node) {
    node->$->prev = this;
    node->$->next = $->next;

    if ($->next) {
        $->next->$->prev = node;
    }
    $->next = node;
}

void Fragment::remove() {
    if ($->next) {
        $->next->$->prev = $->prev;
    }
    if ($->prev) {
        $->prev->$->next = $->next;
    }
}

void TextNodePrivate::preinitText(
  AppNode * app, Fragment * parent, IValue<std::string> * text) {
    node =
      emscripten::val::global("document")
        .call<emscripten::val, const std::string &>("createTextNode", *text);

    preinit(app, parent);

    bindings.insert(new VoidExpression<void, std::string>(
      [this](const std::string & v) {
          node.call<void>("replaceData", 0, -1, v);
      },
      true, text));
}

TextNode::TextNode(TextNodePrivate * $)
    : Fragment($)
    , $($) {}

TextNode::~TextNode() {
    node.call<void>("remove");
}

void TextNode::preinit(
  AppNode * app, Fragment * parent, IValue<std::string> * text) {
    $->preinitText(app, parent, text);
}

emscripten::val * TextNode::findFirstChild() {
    return &node;
}

INode::INode(INodePrivate * $)
    : Fragment($)
    , $($) {}

unsigned int counter = 0;

INode::~INode() {
    for (auto ptr : handlers) {
        delete ptr;
    }
}

void INode::addClass(const std::string & cl) {
    $->app->run->addClass($->node, cl);
}

void INode::addClasses(const std::string & cl) {
    $->app->run->addClass($->node, cl);
}

void INode::bindClass(IValue<std::string> * cl) {
    $->bindings.insert(new DynamicClassBinding(this, cl));
}

void INode::bindClass(IValue<bool> * bind, const std::string & cl) {
    $->bindings.insert(new StaticClassBinding(this, cl, bind));
}

std::string INode::id() {
    auto id = $->node["id"].as<std::string>();

    if (id.empty()) {
        id = "vasille_" + std::to_string(++counter);
        $->node.set("id", id);
    }
    return id;
}

int mouseEventHandler(
  int eventType, const EmscriptenMouseEvent * mouseEvent, void * userData) {
    auto handler =
      reinterpret_cast<std::function<bool(const EmscriptenMouseEvent *)> *>(
        userData);
    return (*handler)(mouseEvent);
}

// void INode::oncontextmenu(ME handler, bool useCapture) {
//     listen("contextmenu", handler, useCapture);
// }

// void INode::oncontextmenu(ME handler, const EventListenerOptions & options) {
//     listen("contextmenu", handler, options);
// }

void INode::onmousedown(ME handler, bool useCapture) {
    emscripten_set_mousedown_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onmousedown(ME handler, const EventListenerOptions & options) {
//     listen("mousedown", handler, options);
// }

void INode::onmouseenter(ME handler, bool useCapture) {
    emscripten_set_mouseenter_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onmouseenter(ME handler, const EventListenerOptions & options) {
//     listen("mouseenter", handler, options);
// }

void INode::onmouseleave(ME handler, bool useCapture) {
    emscripten_set_mouseleave_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onmouseleave(ME handler, const EventListenerOptions & options) {
//     listen("mouseleave", handler, options);
// }

void INode::onmousemove(ME handler, bool useCapture) {
    emscripten_set_mousemove_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onmousemove(ME handler, const EventListenerOptions & options) {
//     listen("mousemove", handler, options);
// }

void INode::onmouseout(ME handler, bool useCapture) {
    emscripten_set_mouseout_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onmouseout(ME handler, const EventListenerOptions & options) {
//     listen("mouseout", handler, options);
// }

void INode::onmouseover(ME handler, bool useCapture) {
    emscripten_set_mouseover_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onmouseover(ME handler, const EventListenerOptions & options) {
//     listen("mouseover", handler, options);
// }

void INode::onmouseup(ME handler, bool useCapture) {
    emscripten_set_mouseup_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onmouseup(ME handler, const EventListenerOptions & options) {
//     listen("mouseup", handler, options);
// }

void INode::onclick(ME handler, bool useCapture) {
    emscripten_set_click_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::onclick(ME handler, const EventListenerOptions & options) {
//     listen("click", handler, options);
// }

void INode::ondblclick(ME handler, bool useCapture) {
    emscripten_set_dblclick_callback(
      id().data(), handle(handler), useCapture, &mouseEventHandler);
}

// void INode::ondblclick(ME handler, const EventListenerOptions & options) {
//     listen("dblclick", handler, options);
// }

int focusEventHandler(
  int eventType, const EmscriptenFocusEvent * mouseEvent, void * userData) {
    auto handler =
      reinterpret_cast<std::function<bool(const EmscriptenFocusEvent *)> *>(
        userData);
    return (*handler)(mouseEvent);
}

void INode::onblur(FE handler, bool useCapture) {
    emscripten_set_blur_callback(
      id().data(), handle(handler), useCapture, &focusEventHandler);
}

// void INode::onblur(FE handler, const EventListenerOptions & options) {
//     listen("blur", handler, options);
// }

void INode::onfocus(FE handler, bool useCapture) {
    emscripten_set_focus_callback(
      id().data(), handle(handler), useCapture, &focusEventHandler);
}

// void INode::onfocus(FE handler, const EventListenerOptions & options) {
//     listen("focus", handler, options);
// }

void INode::onfocusin(FE handler, bool useCapture) {
    emscripten_set_focusin_callback(
      id().data(), handle(handler), useCapture, &focusEventHandler);
}

// void INode::onfocusin(FE handler, const EventListenerOptions & options) {
//     listen("focusin", handler, options);
// }

void INode::onfocusout(FE handler, bool useCapture) {
    emscripten_set_focusout_callback(
      id().data(), handle(handler), useCapture, &focusEventHandler);
}

// void INode::onfocusout(FE handler, const EventListenerOptions & options) {
//     listen("focusout", handler, options);
// }

int keyboardEventHandler(
  int eventType, const EmscriptenKeyboardEvent * mouseEvent, void * userData) {
    auto handler =
      reinterpret_cast<std::function<bool(const EmscriptenKeyboardEvent *)> *>(
        userData);
    return (*handler)(mouseEvent);
}

void INode::onkeydown(KE handler, bool useCapture) {
    emscripten_set_keydown_callback(
      id().data(), handle(handler), useCapture, &keyboardEventHandler);
}

// void INode::onkeydown(KE handler, const EventListenerOptions & options) {
//     listen("keydown", handler, options);
// }

void INode::onkeyup(KE handler, bool useCapture) {
    emscripten_set_keyup_callback(
      id().data(), handle(handler), useCapture, &keyboardEventHandler);
}

// void INode::onkeyup(KE handler, const EventListenerOptions & options) {
//     listen("keyup", handler, options);
// }

void INode::onkeypress(KE handler, bool useCapture) {
    emscripten_set_keypress_callback(
      id().data(), handle(handler), useCapture, &keyboardEventHandler);
}

// void INode::onkeypress(KE handler, const EventListenerOptions & options) {
//     listen("keypress", handler, options);
// }

int touchEventHandler(
  int eventType, const EmscriptenTouchEvent * mouseEvent, void * userData) {
    auto handler =
      reinterpret_cast<std::function<bool(const EmscriptenTouchEvent *)> *>(
        userData);
    return (*handler)(mouseEvent);
}

void INode::ontouchstart(TE handler, bool useCapture) {
    emscripten_set_touchstart_callback(
      id().data(), handle(handler), useCapture, &touchEventHandler);
}

// void INode::ontouchstart(TE handler, const EventListenerOptions & options) {
//     listen("touchstart", handler, options);
// }

void INode::ontouchmove(TE handler, bool useCapture) {
    emscripten_set_touchmove_callback(
      id().data(), handle(handler), useCapture, &touchEventHandler);
}

// void INode::ontouchmove(TE handler, const EventListenerOptions & options) {
//     listen("touchmove", handler, options);
// }

void INode::ontouchend(TE handler, bool useCapture) {
    emscripten_set_touchend_callback(
      id().data(), handle(handler), useCapture, &touchEventHandler);
}

// void INode::ontouchend(TE handler, const EventListenerOptions & options) {
//     listen("touchend", handler, options);
// }

void INode::ontouchcancel(TE handler, bool useCapture) {
    emscripten_set_touchcancel_callback(
      id().data(), handle(handler), useCapture, &touchEventHandler);
}

// void INode::ontouchcancel(TE handler, const EventListenerOptions & options) {
//     listen("touchcancel", handler, options);
// }

int wheelEventHandler(
  int eventType, const EmscriptenWheelEvent * mouseEvent, void * userData) {
    auto handler =
      reinterpret_cast<std::function<bool(const EmscriptenWheelEvent *)> *>(
        userData);
    return (*handler)(mouseEvent);
}

void INode::onwheel(WE handler, bool useCapture) {
    emscripten_set_wheel_callback(
      id().data(), handle(handler), useCapture, &wheelEventHandler);
}

// void INode::onwheel(WE handler, const EventListenerOptions & options) {
//     listen("wheel", handler, options);
// }

// void INode::onabort(PrE handler, bool useCapture) {
//     listen("abort", handler, useCapture);
// }

// void INode::onabort(PrE handler, const EventListenerOptions & options) {
//     listen("abort", handler, options);
// }

// void INode::onerror(PrE handler, bool useCapture) {
//     listen("error", handler, useCapture);
// }

// void INode::onerror(PrE handler, const EventListenerOptions & options) {
//     listen("error", handler, options);
// }

// void INode::onload(PrE handler, bool useCapture) {
//     listen("load", handler, useCapture);
// }

// void INode::onload(PrE handler, const EventListenerOptions & options) {
//     listen("load", handler, options);
// }

// void INode::onloadend(PrE handler, bool useCapture) {
//     listen("loadend", handler, useCapture);
// }

// void INode::onloadend(PrE handler, const EventListenerOptions & options) {
//     listen("loadend", handler, options);
// }

// void INode::onloadstart(PrE handler, bool useCapture) {
//     listen("loadstart", handler, useCapture);
// }

// void INode::onloadstart(PrE handler, const EventListenerOptions & options) {
//     listen("loadstart", handler, options);
// }

// void INode::onprogress(PrE handler, bool useCapture) {
//     listen("progress", handler, useCapture);
// }

// void INode::onprogress(PrE handler, const EventListenerOptions & options) {
//     listen("progress", handler, options);
// }

// void INode::ontimeout(PrE handler, bool useCapture) {
//     listen("timeout", handler, useCapture);
// }

// void INode::ontimeout(PrE handler, const EventListenerOptions & options) {
//     listen("timeout", handler, options);
// }

// void INode::ondrag(DE handler, bool useCapture) {
//     listen("drag", handler, useCapture);
// }

// void INode::ondrag(DE handler, const EventListenerOptions & options) {
//     listen("drag", handler, options);
// }

// void INode::ondragend(DE handler, bool useCapture) {
//     listen("dragend", handler, useCapture);
// }

// void INode::ondragend(DE handler, const EventListenerOptions & options) {
//     listen("dragend", handler, options);
// }

// void INode::ondragenter(DE handler, bool useCapture) {
//     listen("dragenter", handler, useCapture);
// }

// void INode::ondragenter(DE handler, const EventListenerOptions & options) {
//     listen("dragenter", handler, options);
// }

// void INode::ondragexit(DE handler, bool useCapture) {
//     listen("dragexit", handler, useCapture);
// }

// void INode::ondragexit(DE handler, const EventListenerOptions & options) {
//     listen("dragexit", handler, options);
// }

// void INode::ondragleave(DE handler, bool useCapture) {
//     listen("dragleave", handler, useCapture);
// }

// void INode::ondragleave(DE handler, const EventListenerOptions & options) {
//     listen("dragleave", handler, options);
// }

// void INode::ondragover(DE handler, bool useCapture) {
//     listen("dragover", handler, useCapture);
// }

// void INode::ondragover(DE handler, const EventListenerOptions & options) {
//     listen("dragover", handler, options);
// }

// void INode::ondragstart(DE handler, bool useCapture) {
//     listen("dragstart", handler, useCapture);
// }

// void INode::ondragstart(DE handler, const EventListenerOptions & options) {
//     listen("dragstart", handler, options);
// }

// void INode::ondrop(DE handler, bool useCapture) {
//     listen("drop", handler, useCapture);
// }

// void INode::ondrop(DE handler, const EventListenerOptions & options) {
//     listen("drop", handler, options);
// }

// void INode::onpointerover(PoE handler, bool useCapture) {
//     listen("pointerover", handler, useCapture);
// }

// void INode::onpointerover(PoE handler, const EventListenerOptions & options)
// {
//     listen("pointerover", handler, options);
// }

// void INode::onpointerenter(PoE handler, bool useCapture) {
//     listen("pointerenter", handler, useCapture);
// }

// void INode::onpointerenter(PoE handler, const EventListenerOptions & options)
// {
//     listen("pointerenter", handler, options);
// }

// void INode::onpointerdown(PoE handler, bool useCapture) {
//     listen("pointerdown", handler, useCapture);
// }

// void INode::onpointerdown(PoE handler, const EventListenerOptions & options)
// {
//     listen("pointerdown", handler, options);
// }

// void INode::onpointermove(PoE handler, bool useCapture) {
//     listen("pointermove", handler, useCapture);
// }

// void INode::onpointermove(PoE handler, const EventListenerOptions & options)
// {
//     listen("pointermove", handler, options);
// }

// void INode::onpointerup(PoE handler, bool useCapture) {
//     listen("pointerup", handler, useCapture);
// }

// void INode::onpointerup(PoE handler, const EventListenerOptions & options) {
//     listen("pointerup", handler, options);
// }

// void INode::onpointercancel(PoE handler, bool useCapture) {
//     listen("pointercancel", handler, useCapture);
// }

// void INode::onpointercancel(PoE handler, const EventListenerOptions &
// options) {
//     listen("pointercancel", handler, options);
// }

// void INode::onpointerout(PoE handler, bool useCapture) {
//     listen("pointerout", handler, useCapture);
// }

// void INode::onpointerout(PoE handler, const EventListenerOptions & options) {
//     listen("pointerout", handler, options);
// }

// void INode::onpointerleave(PoE handler, bool useCapture) {
//     listen("pointerleave", handler, useCapture);
// }

// void INode::onpointerleave(PoE handler, const EventListenerOptions & options)
// {
//     listen("pointerleave", handler, options);
// }

// void INode::ongotpointercapture(PoE handler, bool useCapture) {
//     listen("gotpointercapture", handler, useCapture);
// }

// void INode::ongotpointercapture(
//   PoE handler, const EventListenerOptions & options) {
//     listen("gotpointercapture", handler, options);
// }

// void INode::onlostpointercapture(PoE handler, bool useCapture) {
//     listen("lostpointercapture", handler, useCapture);
// }

// void INode::onlostpointercapture(
//   PoE handler, const EventListenerOptions & options) {
//     listen("lostpointercapture", handler, options);
// }

// void INode::onanimationstart(AE handler, bool useCapture) {
//     listen("animationstart", handler, useCapture);
// }

// void INode::onanimationstart(AE handler, const EventListenerOptions &
// options) {
//     listen("animationstart", handler, options);
// }

// void INode::onanimationend(AE handler, bool useCapture) {
//     listen("animationend", handler, useCapture);
// }

// void INode::onanimationend(AE handler, const EventListenerOptions & options)
// {
//     listen("animationend", handler, options);
// }

// void INode::onanimationiteraton(AE handler, bool useCapture) {
//     listen("animationiteraton", handler, useCapture);
// }

// void INode::onanimationiteraton(
//   AE handler, const EventListenerOptions & options) {
//     listen("animationiteraton", handler, options);
// }

// void INode::onclipboardchange(CE handler, bool useCapture) {
//     listen("clipboardchange", handler, useCapture);
// }

// void INode::onclipboardchange(
//   CE handler, const EventListenerOptions & options) {
//     listen("clipboardchange", handler, options);
// }

// void INode::oncut(CE handler, bool useCapture) {
//     listen("cut", handler, useCapture);
// }

// void INode::oncut(CE handler, const EventListenerOptions & options) {
//     listen("cut", handler, options);
// }

// void INode::oncopy(CE handler, bool useCapture) {
//     listen("copy", handler, useCapture);
// }

// void INode::oncopy(CE handler, const EventListenerOptions & options) {
//     listen("copy", handler, options);
// }

// void INode::onpaste(CE handler, bool useCapture) {
//     listen("paste", handler, useCapture);
// }

// void INode::onpaste(CE handler, const EventListenerOptions & options) {
//     listen("paste", handler, options);
// }

void INode::bindShow(IValue<bool> * value) {
    auto lastDisplay = std::make_shared<std::string>("");

    *lastDisplay = $->node["style"]["display"].as<std::string>();

    this->bindAlive(
      value,
      [=]() {
          *lastDisplay = $->node["style"]["display"].as<std::string>();
          $->node["style"].set("display", "none");
      },
      [=]() { $->node["style"].set("display", lastDisplay->c_str()); });
}

void INode::html(IValue<std::string> * value) {

    $->node.set("innerHTML", std::string(*value).c_str());

    watch(
      {[=](const std::string & v) { $->node.set("innerHTML", v.c_str()); }},
      value);
}

Tag::Tag(TagPrivate * $)
    : INode($)
    , $($) {}

Tag::~Tag() {
    $->node.call<void>("remove");
}

void Tag::preinit(
  AppNode * app, Fragment * parent, const std::string & tagName) {
    auto node = emscripten::val::global("document")
                  .call<emscripten::val>("createElement", tagName);

    $->preinit(app, parent);
    $->node = node;

    $->parent->appendNode(node);
}

void Tag::bindMount(IValue<bool> * cond) {
    bindAlive(
      cond,
      [=]() {
          $->node.call<void>("remove");
          $->unmounted = true;
      },
      [=]() {
          if ($->unmounted) {
              insertAdjacent($->node);
              $->unmounted = false;
          }
      });
}

emscripten::val * Tag::findFirstChild() {
    return $->unmounted ? nullptr : &$->node;
}



void Tag::insertAdjacent(emscripten::val & node) {
    if ($->unmounted) {
        if ($->next) {
            $->next->insertAdjacent(node);
        }
        else {
            $->parent->appendNode(node);
        }
    }
    else {
        INode::insertAdjacent(node);
    }
}

Extension::Extension(ExtensionPrivate * $)
    : INode($)
    , $($) {}

void Extension::preinit(AppNode * app, Fragment * parent0) {
    auto parent = dynamic_cast<INode *>(parent0);

    if (parent) {
        $->preinit(app, parent);
        $->node = parent->node();
    }
    else {
        throw userError(
          "A extension node can be encapsulated only in a "
          "tag/extension/component",
          "virtual-dom");
    }
}

Component::Component(ComponentPrivate * $)
    : Extension($)
    , $($) {}

void Component::preinit(AppNode * app, Fragment * parent) {
    $->preinit(app, parent);
}

void Component::mounted() {
    if (children.size() != 1) {
        throw userError("Component must have a child only", "dom-error");
    }
    auto child  = children[0];
    auto asTag  = dynamic_cast<Tag *>(child);
    auto asComp = dynamic_cast<Component *>(child);

    if (asTag) {
        $->node = asTag->node();
    }
    else if (asComp) {
        $->node = asComp->node();
    }
    else {
        throw userError(
          "Component child must be Tag or Component", "dom-error");
    }
}

SwitchedNode::SwitchedNode(SwitchedNodePrivate * $)
    : Fragment($)
    , $($) {

    $->sync = [=](const bool &) {
        int i = 0;

        for (; i < $->cases.size(); i++) {
            if (*$->cases[i].cond) {
                break;
            }
        }

        if (i == $->index) {
            return;
        }

        if ($->fragment) {
            delete $->fragment;
            children.clear();
            $->fragment = nullptr;
        }

        if (i != $->cases.size()) {
            $->index = i;
            createChild($->cases[i].cb);
        }
        else {
            $->index = -1;
        }
    };
}

SwitchedNode::~SwitchedNode() {
    for (auto & c : $->cases) {
        c.cond->off($->sync);
    }
}

void SwitchedNode::addCase(const Case & case$) {
    $->cases.push_back(case$);
}

void SwitchedNode::createChild(std::function<void(Fragment *)> cb) {
    auto node = new Fragment();

    node->preinit($->app, this);
    node->init();

    $->fragment = node;
    children.push_back(node);

    cb(node);
}

void SwitchedNode::ready() {
    for (auto & c : $->cases) {
        c.cond->on($->sync);
    }
}

DebugPrivate::~DebugPrivate() {
    node.call<void>("remove");
}

void DebugPrivate::preinit(
  AppNode * app, Fragment * parent, IValue<std::string> * text) {
    FragmentPrivate::preinit(app, parent);
    node = emscripten::val::global("document")
             .call<emscripten::val>(
               "createComment", static_cast<const std::string &>(*text));

    bindings.insert(new VoidExpression<void, std::string>(
      [=](const std::string & v) { node.call<void>("replaceData", 0, -1, v); },
      true, text));

    parent->appendNode(node);
}
