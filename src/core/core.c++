#include <core/core.h++>
#include <core/errors.h++>


ReactivePrivate::~ReactivePrivate() {
    for (auto it : watch) {
        delete it;
    }

    for (auto it : bindings) {
        delete it;
    }

    for (auto it : models) {
        delete it;
    }
}

Reactive::Reactive(ReactivePrivate * $)
    : $($) {}

Reactive::~Reactive() {
    delete $;
}

void Reactive::enable() {
    if (!$->enabled) {
        for (auto & watcher : $->watch) {
            watcher->enable();
        }
        for (auto & model : $->models) {
            model->enableReactivity();
        }
        $->enabled = true;
    }
}

void Reactive::disable() {
    if ($->enabled) {
        for (auto & watcher : $->watch) {
            watcher->disable();
        }
        for (auto & model : $->models) {
            model->disableReactivity();
        }
        $->enabled = false;
    }
}

void Reactive::bindAlive(
  IValue<bool> * cond, std::function<void()> onOff,
  std::function<void()> onOn) {

    if ($->freezeExpr) {
        throw wrongBinding("this component already have a freeze state");
    }

    if ($->watch.find(cond) != $->watch.end()) {
        throw wrongBinding(
          "freeze state must be bo"
          "und to an external component");
    }

    $->freezeExpr = new VoidExpression<void, bool>(
      [=](const bool & state) {
          $->frozen = !state;

          if (state) {
              if (onOn) {
                  onOn();
              }
              for (auto & watcher : $->watch) {
                  watcher->enable();
              }
          }
          else {
              if (onOff) {
                  onOff();
              }
              for (auto & watcher : $->watch) {
                  watcher->disable();
              }
          }
      },
      true, cond);
}
