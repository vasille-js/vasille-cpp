#include <core/errors.h++>
#include <iostream>

const char * reportIt =
  "Report it here: https://gitlab.com/vasille-js/vasille-js/-/issues";

std::string notOverwritten() {
    std::cout << "Vasille.js: Internal error "
              << "Must be overwritten " << reportIt;
    return "not-overwritten";
}

std::string internalError(const std::string & msg) {
    std::cout << "Vasille.js: Internal error " << msg << ' ' << reportIt;
    return "internal-error";
}

std::string userError(const std::string & msg, const std::string & err) {
    std::cout << "Vasille.js: User error " << msg;
    return err;
}

std::string wrongBinding(const std::string & msg) {
    return userError(msg, "wrong-binding");
}
