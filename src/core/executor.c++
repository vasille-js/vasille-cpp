#include <core/executor.h++>
#include <emscripten/html5.h>



void InstantExecutor::addClass(emscripten::val & el, const std::string & cl) {
    if (!el.isUndefined()) {
        el["classList"].call<void>("add", emscripten::val(cl));
    }
}

void InstantExecutor::removeClass(
  emscripten::val & el, const std::string & cl) {
    if (!el.isUndefined()) {
        el["classList"].call<void>("remove", emscripten::val(cl));
    }
}

void InstantExecutor::setAttribute(
  emscripten::val & el, const std::string & name, const std::string & value) {
    el.call<void>(
      "setAttribute", emscripten::val(name), emscripten::val(value));
}

void InstantExecutor::removeAttribute(
  emscripten::val & el, const std::string & name) {
    el.call<void>("removeAttribute", emscripten::val(name));
}

void InstantExecutor::setStyle(
  emscripten::val & el, const std::string & prop, const std::string & value) {
    el["style"].call<void>(
      "setProperty", emscripten::val(prop), emscripten::val(value));
}

void InstantExecutor::insertBefore(
  emscripten::val & el, emscripten::val & child, emscripten::val & before) {
    el.call<void>("insertBefore", child, before);
}

void InstantExecutor::appendChild(
  emscripten::val & el, emscripten::val & child) {
    el.call<void>("appendChild", child);
}

void InstantExecutor::callCallback(const std::function<void()> & cb) {
    cb();
}

InstantExecutor * InstantExecutor::instance() {
    static InstantExecutor instance;

    return &instance;
}

void timeoutHandler(void * data) {
    auto func = reinterpret_cast<std::function<void()> *>(data);
    (*func)();
    delete func;
}

void setTimeout(std::function<void()> * callback) {
    emscripten_set_timeout(&timeoutHandler, 0, callback);
}

void TimeoutExecutor::addClass(emscripten::val & el, const std::string & cl) {
    callCallback([=, &el]() { InstantExecutor::addClass(el, cl); });
}

void TimeoutExecutor::removeClass(
  emscripten::val & el, const std::string & cl) {
    callCallback([=, &el]() { InstantExecutor::removeClass(el, cl); });
}

void TimeoutExecutor::setAttribute(
  emscripten::val & el, const std::string & name, const std::string & value) {
    callCallback(
      [=, &el]() { InstantExecutor::setAttribute(el, name, value); });
}

void TimeoutExecutor::removeAttribute(
  emscripten::val & el, const std::string & name) {
    callCallback([=, &el]() { InstantExecutor::removeAttribute(el, name); });
}

void TimeoutExecutor::setStyle(
  emscripten::val & el, const std::string & prop, const std::string & value) {
    callCallback([=, &el]() { InstantExecutor::setStyle(el, prop, value); });
}

void TimeoutExecutor::insertBefore(
  emscripten::val & el, emscripten::val & child, emscripten::val & before) {
    callCallback(
      [&, this]() { InstantExecutor::insertBefore(el, child, before); });
}

void TimeoutExecutor::appendChild(
  emscripten::val & el, emscripten::val & child) {
    callCallback([&, this]() { InstantExecutor::appendChild(el, child); });
}

void TimeoutExecutor::callCallback(const std::function<void()> & cb) {
    setTimeout(new std::function<void()>([=]() { cb(); }));
}

TimeoutExecutor * TimeoutExecutor::instance() {
    static TimeoutExecutor timeoutExecutor;
    return &timeoutExecutor;
}
